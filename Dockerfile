FROM alpine:3.12 

MAINTAINER "DeX"

RUN apk add --no-cache --virtual .run-deps rsync \
					   bash \
                                           openssh \
                                           tzdata \
                                           curl \
                                           ca-certificates \
 && update-ca-certificates \
 && rm -rf /var/cache/apk/*
COPY docker-entrypoint.sh /

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["bash"]
